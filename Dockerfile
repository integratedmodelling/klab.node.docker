FROM openjdk:8u272-jdk-buster as node:latest
#--Weather Node Production Build
ARG NODE_JAR=klab.node.jar
ARG CERT=/run/secrets/node_secret.cert
ARG CONFIG=/opt/node/bootstrap.yml
ARG SYSTEM_USER=node

ENV NODE_JAR=$NODE_JAR
ENV CONFIG=$CONFIG
ENV CERT=$CERT

ENV WFS_AUTH=*
ENV FSCAN_AUTH=*
ENV SEARCH_AUTH=*
ENV RASTER_AUTH=*
ENV VECTOR_AUTH=*
ENV SUBMITTING_AUTH=ARIES
ENV WEATHER_AUTH=*
ENV GEOSERVER_URL=geoserver:8080/geoserver
ENV GEOSERVER_USER=admin
ENV GEOSERVER_PASSWORD=somepassword
ENV POSTGRES_HOST=postgres
ENV POSTGRES_PORT=5432
ENV POSTGRES_USER=postgres
ENV POSTGRES_PASSWORD=somepassword
ENV CONTEXT_PATH=/someapplicationcontext
ENV MAX_UPLOAD=3000MB
ENV MAX_REQUEST=3000MB
ENV DEBUG_PORT=9999

RUN groupadd -g 1000 $SYSTEM_USER && useradd -u 1000 -g $SYSTEM_USER -s /bin/sh $SYSTEM_USER && \
    usermod -aG $SYSTEM_USER,adm root && \
    mkdir -p /opt/$SYSTEM_USER && \
    mkdir -p /home/$SYSTEM_USER/.klab && \
    mkdir -p /var/log/$SYSTEM_USER && \
    chown -R $SYSTEM_USER:$SYSTEM_USER /opt/$SYSTEM_USER && \
    chown -R $SYSTEM_USER:$SYSTEM_USER /home/$SYSTEM_USER/.klab && \
    chown -R $SYSTEM_USER:$SYSTEM_USER /var/log/$SYSTEM_USER

USER $SYSTEM_USER

COPY ./target/$NODE_JAR /opt/$SYSTEM_USER/$NODE_JAR
COPY ./target/entrypoint.sh /opt/$SYSTEM_USER/entrypoint.sh
COPY ./target/bootstrap.yml $CONFIG

WORKDIR /opt/$SYSTEM_USER
EXPOSE 8287

ENTRYPOINT ["./entrypoint.sh"]
