#!/bin/sh
[ "$DEBUG" = TRUE ] &&
  export JAVA_TOOL_OPTIONS=-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=$DEBUG_PORT

/usr/local/openjdk-8/bin/java -Xmx4048M \
              -Dklab.adapter.wcs.auth=$WCS_AUTH \
              -Dklab.adapter.fscan.auth=$FSCAN_AUTH \
              -Dklab.adapter.wfs.auth=$WFS_AUTH \
              -Dklab.adapter.raster.auth=$RASTER_AUTH \
              -Dklab.adapter.vector.auth=$VECTOR_AUTH \
              -Dklab.adapter.weather.auth=$WEATHER_AUTH \
              -Dklab.node.searching=$SEARCH_AUTH \
              -Dklab.node.submitting=$SUBMITTING_AUTH \
              -Dklab.geoserver.url=$GEOSERVER_URL \
              -Dklab.geoserver.user=$GEOSERVER_USER \
              -Dklab.geoserver.password=$GEOSERVER_PASSWORD \
              -Dklab.postgres.host=$POSTGRES_HOST \
              -Dklab.postgres.port=$POSTGRES_PORT \
              -Dklab.postgres.user=$POSTGRES_USER \
              -Dklab.postgres.password=$POSTGRES_PASSWORD \
              -Dserver.servlet.contextPath=$CONTEXT_PATH \
              -Dspring.servlet.multipart.max-file-size=$MAX_UPLOAD \
              -Dspring.servlet.multipart.max-request-size=$MAX_REQUEST \
              -jar $NODE_JAR \
              -cert $CERT
